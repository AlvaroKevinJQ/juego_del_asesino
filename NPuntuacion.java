
public class NPuntuacion {
	private NPuntuacion der, izq;
	private int info;
	
	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

	public NPuntuacion getDer() {
		return der;
	}

	public void setDer(NPuntuacion der) {
		this.der = der;
	}

	public NPuntuacion getIzq() {
		return izq;
	}

	public void setIzq(NPuntuacion izq) {
		this.izq = izq;
	}

}
