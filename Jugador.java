public class Jugador{
        
        public String nombre, rol = "Inocente"; //Declaramos las propiedades del objeto jugador.
        public int estado = 1, puntuacion = 0; //Estado "1" = vivo; "0" = muerto; "-1" = m�s de una ronda muerto
        //public MPunArb arbol = new MPunArb();
        public Jugador ()
        {
        }
        public Jugador (String nom){
        	
        	nombre = nom;
        }
        //Declaramos los m�todos que cada jugador realizar� dependiendo su rol
        
        public void setName(String name)
        {
        	nombre = name;
        }
        public void setRole(String rl)
        {
        	rol = rl;
        }
        public void setStatus(int sta)
        {
        	estado = sta;
        }
        public String getName()
        {
        	return nombre;
        }
        public String getRole()
        {
        	return rol;
        }
        public int getStatus()
        {
        	return estado;
        }
        /*public int getPuntuacion() {
        	puntuacion = arbol.getSuma();
        	return puntuacion;
        }
        public void setPuntuacion(int pun) {
        	puntuacion += pun;
        }
        public void addPuntos(int pun) {
        	arbol.Insertar(pun);
        }*/
}