
public class MPunArb {
	static NPuntuacion nuevo = new NPuntuacion();
	 NPuntuacion raiz = null, p = null, q, r, otro;
	 int suma = 0;
	
	public void insertar(int pun) {
		if(p == null) {
			
			NPuntuacion nuevo = new NPuntuacion();
			nuevo.setInfo(pun); 
			nuevo.setIzq(null); nuevo.setDer(null);
			raiz = nuevo; p = raiz;
		}
		else {
			nuevo = null;
			
			if(pun < p.getInfo()) {
				
				if(p.getIzq() == null) {
					
					NPuntuacion nuevo = new NPuntuacion();
					nuevo.setIzq(null); nuevo.setDer(null); nuevo.setInfo(pun); p.setIzq(nuevo);
					//System.out.println("Dato insertado....");
					
				}//If_3
				else { p = p.getIzq(); insertar(pun); }
				
			}//If_2
			
			else { 
				
				if(pun >= p.getInfo()) {
					
					if(p.getDer() == null) {
						
						NPuntuacion nuevo = new NPuntuacion();
						nuevo.setIzq(null); nuevo.setDer(null); nuevo.setInfo(pun); p.setDer(nuevo);
						//System.out.println("Dato insertado....");
						
					}//If_5
					
					else { p = p.getDer(); insertar(pun); }//else_3
					
				}//If_4
				
				//else System.out.println("El nodo ya se encuentra en el �rbol...");
				
			}//else_2
			
			p = raiz;
		}
	}
	public void sumaP() {
		
		NPuntuacion aux = raiz;
		sumaPre(aux);
		
	}//M�todo Aux_Pre
	
	public void sumaPre(NPuntuacion nodo) {
		
		if(nodo != null) {
			
			suma += nodo.getInfo();
			sumaPre(nodo.getIzq());
			sumaPre(nodo.getDer());
			
		}
		
	}//M�todo Pre
	public int getSuma() {
		sumaP();
		return suma;
	}
	
	public int getPuntos() {
		return suma;
	}

}


