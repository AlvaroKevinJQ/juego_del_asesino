import java.util.Scanner;

public class ListaVotacion{
	static Scanner leer = new Scanner(System.in);
	static NodoJugador nuevo = new NodoJugador();
	NodoJugador fina;
	NodoJugador inicio = null, p = null, q = null;
	public static int contador = 0;
	
	
	public void InsertarPorElFinal(String nombre){
		
		NodoJugador nuevo = new NodoJugador();
	    /*
		nuevo.SetNom(nombre);
		nuevo.sig = null;
		
		if(inicio == null){
			 inicio = nuevo;
			}
		else p.sig = nuevo;
		p = nuevo; 
		nuevo = null;
		contador++;
		System.out.println(inicio.getNom());
		//Metodo para crear un nuevo nodo apartir del ultimo
		*/
			if(inicio == null){  
					nuevo.SetNom(nombre);
					inicio = nuevo;
					fina = inicio;
			}else{
					fina.sig = nuevo;
					nuevo.SetNom(nombre);
					fina = nuevo;
					fina.sig = null;
			}
			//System.out.println(nuevo.getNom());
	}//M�todo InsertarPorElFinal

	
	public void ConsultarVotos(int jugadores){
		
		if(inicio == null){
			System.out.println("Lista vacia");
		}else{
			
			q = inicio;
			//System.out.println(inicio + "Nombre: "+ inicio.getNom());
			do{ 
				
				System.out.println("�Cuantos jugadores votan por "+ q.getNom()+" ?");
				q.SetVoto(validarEntrada(jugadores));
				q = q.sig;
				
			}while(q != null);//while
				
		}//else
		
	}//M�todo ConsultarVotos
	
	public String[] mayorVotos(int jugadores){
		
		int mayor = 0, i = 0;
		String arrMayor[] = new String[jugadores];
		for (int x = 0; x < arrMayor.length; x++) {
			arrMayor[x] = null;
		}
		if(inicio == null){
			System.out.println("Lista vacia");
		}
		else {
			q = inicio;
			while(q != null) {
				if(mayor <= q.getVoto()) mayor = q.getVoto();
				q = q.sig;
			}//while
			System.out.printf("El mayor de votos es: %d\n", mayor);
		}//else
		q=inicio;
		System.out.println("Los jugadores con dicha cantidad son: ");
		while(q != null) {
			if(mayor == q.getVoto()) {
				System.out.println(q.getNom());
				arrMayor[i] = q.getNom();
				i++;
			}//if
			q = q.sig;
		}//while
		return arrMayor;
		
	}
	
	
	public int validarEntrada(int jugadores){ //Valida el ingreso del nombre del nodo
        boolean s = true; int num;
        do{
                try{
                        num = leer.nextInt();
                        if(num >= 0 && num < jugadores){
                                //System.out.println("Valor valido");
                                s=false;
                                return num;
                        }else
                                System.out.println("limite de jugadores vivos excedido...\n ");
                }catch(Exception e){
                        leer.next();
                        System.out.println("Dato invalido, ingrese nuevamente");
                }
        }while(s);
        return 0;
	}//validar
}//class
