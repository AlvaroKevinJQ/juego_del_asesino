
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Menu 
{
	static Scanner leer = new Scanner (System.in);
	public static void Iniciar() 
	
	{
		
		int opc = 0;
		
		do
	      {
	         System.out.println("\n\nMEN�");
	         System.out.println("[1] Iniciar Juego");
	         System.out.println("[2] �Como jugar?");
	         System.out.println("[3] Cr�ditos");
	         System.out.println("[4] Salir");
	         
	         try {opc = leer.nextInt();}
	         catch(Exception e) {System.out.println("Valor no valido");}
			leer.nextLine();
				
	         switch(opc)
	         {
	         	case 1:
	         		
	         		Desarrollo.Iniciar();
	         		
	         	break;
	         	
	         	case 2: 
	         		
				try {
					
					//FileReader = Lee el conetido de un archivo como cadenas de caracteres. 
					//Uno por uno y regresa un valor entero (-1) cuando ya no hay mas informaci�n que leer.
					
					FileReader lectura = new FileReader("�Como jugar.txt");
					int informacion = lectura.read();
					
					while(informacion != -1) {
						
						System.out.print((char)informacion);
						informacion = lectura.read();
						
					}//while
					
					lectura.close();
					
				}//try
				
				catch (FileNotFoundException e) {e.printStackTrace();} 
				catch (IOException e) {e.printStackTrace();}
	         		
	         	break;
	         	
	         	case 3:
	         		
	         		System.out.println("Creditos\nDesarrolladores:");
	         		System.out.println("Jorge Anotnio Toscano Lara");
	         		System.out.println("Alvaro Kevin Quintero Jimenez");
	         		System.out.println("Luis Ernesto Garcia Alvarez");
	         		
	         	break;
	         	
	         	case 4:
	         		
	         		System.out.println("Saliendo...");
	         		
	         	break;
	         	
	         	default: System.out.println("Opci�n invalida..."); break;
	         }//switch
	         
	      }while(opc!= 4);
		
		System.out.println("Juego terminado...");
	
	}//M�todo Iniciar
	
}//Class Menu
