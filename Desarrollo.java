import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.Random;

public class Desarrollo {
	
	static Scanner read = new Scanner (System.in);
	static int players = noJugadores(), players_aux = players, dios, policia, doctor, asesino;
	static Jugador arJugadores[] = new Jugador[players];
	static MPunArb arArb[] = new MPunArb[players];
	static ListaVotacion Lista = new ListaVotacion();
	static String mayores[] = new String [players];
	
	
	public static void Iniciar()//Inicio del juego
	{
		int rondas = 1;

		//**Aqu� se llena el arreglo con los jugadores y se les asigna un rol
		fillJugadores();
		asignRoles();
		
		//**Muestra primeramente el jugador que va a ser "Dios". Despues muestra los roles de los demas jugadores
		System.out.printf("Judador %d: %s : %s\n", dios + 1, arJugadores[dios].getName(), arJugadores[dios].getRole());
		System.out.println("Presione <<Enter>> cuando este listo para continuar...");
		read.nextLine();
		
		for(int i = 0; i < players; i++)
			if(i != dios)
				System.out.printf("Judador %d: %-10s : %s\n", i + 1, arJugadores[i].getName(), arJugadores[i].getRole());
		
		System.out.println();
		
		//**Comienzan las rondas (Noches)
		do {
			System.out.println("Comienza la etapa " + rondas + " del juego:");
			
			Noche_Amanecer(rondas);
			
			if(arJugadores[asesino].getStatus() == 1) DiscusionVotacion(); 
			for(int i = 0; i < players; i++) 
				if(arJugadores[i].getStatus() == 0) 
					arJugadores[i].setStatus(-1);
			
			rondas++;
			Lista = null;
			
		}while((arJugadores[asesino].getStatus() == 1) && (players_aux - 1 > 2));//Mientras no se encuentre al asesino o haya al menos dos jugadores (contando al asesino) no se detendr� el ciclo
		
		System.out.println("Fin del juego....");
		
		if(arJugadores[asesino].getStatus() == 1) System.out.println("El asesino ha ganado...");
		else System.out.println("Han atrapado al asesino. Felicidades....");
		for(int x = 0; x < players; x++) {
			arArb[x].getSuma();
		}
		quickSort();
		for(int x = 0; x < players; x++) {
			System.out.printf("Jugador %d: %-10s : %d puntos\n", x+1, arJugadores[x].getName(), arArb[x].getPuntos());
		}
		
	}//Iniciar
	
	public static int noJugadores()//Metodo para determinar el numero de jugadores
	{
		int jug=0;
		while(players <5 || players>50) {
			System.out.print("Ingresa la cantidad de jugadores: ");
			jug = validarEntradaJ();
			if(jug < 5)System.out.println("El minimo de jugadores es 5");
			else if(jug>50)System.out.println("El maximo de jugadores es 50");
			else return jug;
		}
		//int jug = 5;
		return jug;
	}//noJugadores
	
	public static void fillJugadores()//llenado de la cantidad de jugadores
	{
		String name;
		read.nextLine();
		
		for(int i = 0; i < players; i++)
		{
			System.out.printf("Ingresa el nombre del jugador %d:", i + 1);
			name = read.nextLine();
			arJugadores[i] = new Jugador(name);
			arArb[i] = new MPunArb();
			//System.out.println(arJugadores[i].getName());
			//arJugadores[i].setName(read.nextLine());
			
		}
		/*arJugadores[0] = new Jugador("Angel");
		arJugadores[1] = new Jugador("Beto");
		arJugadores[2] = new Jugador("Carlos");
		arJugadores[3] = new Jugador("Diego");
		arJugadores[4] = new Jugador("Estefania");
		arArb[0] = new MPunArb();
		arArb[1] = new MPunArb();
		arArb[2] = new MPunArb();
		arArb[3] = new MPunArb();
		arArb[4] = new MPunArb();*/
		
		System.out.println();
	}//fillJugadores
	
	public static void asignRoles ()//Metodo para la asignacion de cada uno de los cuatro roles G=dios D=Doctor P=policia I=Inocente K=Asesino
	
	{
		int rol = 0;
		Random aleatorio = new Random();
		Boolean aRol = false;
		for(int i = 0; i <= 3; i++)
		{
			while(aRol == false)
			{
				rol = aleatorio.nextInt(players);
				if(arJugadores[rol].getRole() == "Inocente")
					aRol = true;
			}
			aRol = false;
			
			if(i == 0) {
				arJugadores[rol].setRole("Dios"); dios = rol;
			}//If_1
			if(i == 1) {
				arJugadores[rol].setRole("Asesino"); asesino = rol;
			}//If_2
			if(i == 2) {
				arJugadores[rol].setRole("Policia"); policia = rol;
			}//If_3
			if(i == 3) {
				arJugadores[rol].setRole("Doctor"); doctor = rol;
			}//If_4
			
		}//for
	}//asignRoles
	
	public static void Noche_Amanecer(int rondas) {
		
		System.out.println(arJugadores[doctor].getStatus());
		int rolN = 0, accionK = 0, accionP = 0, accionD = 0;
		System.out.println("Presione <<Enter>> cuando este listo para continuar...");
		read.nextLine();
		
		System.out.printf("\nEstatus de todos los jugadores antes de la ronda %d\n\n", rondas);
		ImprimirEstatus();
		
		System.out.printf("\nDios, bienvenido a la noche numero %d.\nA continuacion los jugadores con los roles de <<Policia>>, <<Doctor>> y <<Asesino>> daran el nombre del\njugador al cual desean que se le aplique las acciones correspondidas a sus respectivos roles: \n\n", rondas);
		
		accionK = ValidacionAccion(rolN); rolN = 1;
		
		if(arJugadores[doctor].getStatus() == 0 || arJugadores[doctor].getStatus() == 1 ) accionD = ValidacionAccionDoctor();
		else System.out.println("El doctor esta muerto...");
		if(arJugadores[policia].getStatus() == 1 ) accionP = ValidacionAccion(rolN);
		Asesino(accionK);
		Doctor(accionD);
		

		//Amanecer
		
		if(arJugadores[policia].getStatus() == 1) Policia(accionP);
		
		System.out.printf("\nEstatus de todos los jugadores despu�s de la ronda %d\n", rondas);
		ImprimirEstatus();
		
		
	}//Metodo Noche_Amanecer (Los 3 roles de acci�n, ejecutar�n sus roles y se dan las noticias de lo ocurrido la noche anterior)
	
	public static void ImprimirEstatus() {
		
		for(int i = 0; i < players; i++) { 
			String estado = "vivo";
			if(arJugadores[i].getStatus() <= 0) estado = "muerto";
			if (i == asesino) if(arJugadores[asesino].getStatus() == 0) estado = "detenido";
			System.out.printf("Judador %2d: %-10s: %s\n", i + 1, arJugadores[i].getName(), estado);
		}//For_1
		
		System.out.println();
		
	}//M�todo ImprimirEstatus
	
	public static int ValidacionAccion(int rolN) {
		
		String nombres;
		String rol = " ";
		boolean muerto = true;
		if(rolN < 1) rol = "Asesino";
		else rol = "Policia";
		
		do {
			
			System.out.println(rol + ": "); nombres = read.nextLine();
			
			if(arJugadores[dios].getName().equals(nombres)) System.out.println("No puedes matar ni arrestar a DIOS....P.D: Ya lo intento Nietzsche");
			else {
				for(int i = 0; i < players; i++) {
				
					if(rol == "Asesino") if(arJugadores[asesino].getName().equals(nombres)) {System.out.println("No te puedes suicidar..."); break;}
					if(rol == "Policia") if(arJugadores[policia].getName().equals(nombres)) {System.out.println("No te puedes arrestar..."); break;}

					if(arJugadores[i].getName().equals(nombres)) {
						
						if(arJugadores[i].getStatus() < 0) {
							
							System.out.println("No puedes escoger a un muerto..."); muerto = true;
							
						}//If_2
						
						else return i;
						
					}//If_1
					
				}//For_1
			}//Else
			
			}while(arJugadores[dios].getName().equals(nombres) || muerto);
		
		return (Integer) null;
		
	}//M�todo ValidacionAcci�n (Los roles de asesino y p�licia seleccionan sus jugadores)
	
	public static int ValidacionAccionDoctor() {
		
		String nombres;
		boolean muerto = true;
		do {
			
			System.out.println("Doctor: "); nombres = read.nextLine();
			if(arJugadores[dios].getName().equals(nombres)) System.out.println("Usted no puede salvar a DIOS....");
			else {
				for(int i = 0; i < players; i++) {
					
					if(arJugadores[i].getName().equals(nombres)) {						
						if(arJugadores[i].getStatus() < 0) muerto = true;
						else return i;
					}//If_1
					
				}//For_1
				
			}//else
				
		}while(arJugadores[dios].getName().equals(nombres) || muerto);
		
		return (Integer) null;
		
	}//M�todo ValidacionAcci�nDoctor (El rol de doctor selecciona su jugador)
	
	public static void Asesino(int posicion) {
		
		arArb[asesino].insertar(10);
		arJugadores[posicion].setStatus(0); players_aux--;
		System.out.printf("El asesino ha matado a un  jugador\n");
			
	}//M�todo Asesino
	
	public static void Doctor(int posicion) {
		
		if(arJugadores[posicion].getStatus() == 0) {
			arJugadores[posicion].setStatus(1); players_aux++;
			arArb[doctor].insertar(30);
			System.out.println("El doctor ha hecho su trabajo y ha salvado a un jugador");
		}
		else System.out.println("El doctor no ha hecho su trabajo");
		
	}//M�todo Doctor
	
	public static void Policia(int posicion) {
		
		if(posicion == asesino) { 
			arJugadores[asesino].setStatus(0);
			arArb[policia].insertar(50);
			System.out.println("El polica ha atrapado al asesino");
		}
		else System.out.println("El policia no ha hecho su trabajo");
		
		
	}//M�todo Policia
	
	public static void DiscusionVotacion() {
		
		Lista = new ListaVotacion();
		System.out.println("Basado en lo ocurrido en la noche los jugadores tienen 5 minutos para discutir\nsobre el tema y votar a un posible culpable de lo acontesido...");
		System.out.println("El tiempo esta transcurriendo... Presiona enter para comenzar votacion o espera a que el timepo termine");
		tiempo(5);
		//System.out.println("termine");
		int deci=-1;
		int i = 0;
		int mayor=0;
		while(i < players){ 
			
			if(arJugadores[i].estado == 1 && i != dios){ 
				
				Lista.InsertarPorElFinal(arJugadores[i].nombre);
				//System.out.println(arJugadores[i].nombre);
				
			}//if
			
			i++;
			
		}//while
		
		do {
			
			Lista.ConsultarVotos(players_aux);
			for (int x = 0; x < mayores.length; x++) {
				mayores[x] = null;
			}
			mayores = Lista.mayorVotos(players);
			
			tiempo(5);
			System.out.println("En caso de haber un empate y no abrir la votacion, morira una de las personas con mayor cantidad de votos");
			System.out.printf("Quieres abrir otra votacion? [1]SI [2]NO: ");
			deci = validarEntrada();
			
		}while(deci == 1);
		
		if(mayores[1] == null) {
			//System.out.println("entre");
			for(int x = 0; x < arJugadores.length; x++) {
				if(arJugadores[x].getName().equals(mayores[0])) {
					arJugadores[x].setStatus(-1); players_aux--;
				}//if
			}//for
		}//if1
		else{
			int ran = 0;
			Random aleatorio = new Random();
			Boolean rKill = false;
			while(rKill == false)
			{
				ran = aleatorio.nextInt(players);
				if(arJugadores[ran].getStatus() == 1){
					for(int x = 0; x < players; x++) {
						if(arJugadores[ran].getName().equals(mayores[x])) {
							arJugadores[x].setStatus(-1); players_aux--;
							rKill = true;
							System.out.printf("%s ha muerto por votacion...", arJugadores[x].getName());
							break;
						}
					}
				}
			}
		}
		if(arJugadores[asesino].getStatus() == -1) {
			for(int x = 0; x < players; x++) {
				if(!arJugadores[x].getRole().equals("Asesino")) arArb[x].insertar(30);
			}
		}

		
		
	}//class Discusion
	
	public static void tiempo(int t) {
		
		long timeInicio = System.currentTimeMillis(); long timeISeconds = TimeUnit.MILLISECONDS.toSeconds(timeInicio);
		long cTimeMillis = System.currentTimeMillis(); long cTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(cTimeMillis); 
		
		do {
			
			cTimeMillis = System.currentTimeMillis(); cTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(cTimeMillis);
			
		}while(cTimeSeconds != timeISeconds + t);//la suma representa la cantidad de sgundos que estableceremos para la discusion.
	
	}//Metodo tiempo
	
	public static int validarEntrada(){ //Valida el ingreso del nombre del nodo
		
        boolean s = true; int num;
        
        do{
                try{
                        num = read.nextInt();
                        if(num >= 1 && num <= 2){
                                //System.out.println("Valor valido");
                                s = false;
                                return num;
                        }else
                                System.out.println("Numero invalido, ingrese nuevamente");
                }catch(Exception e){
                        read.next();
                        System.out.println("Dato invalido, ingrese nuevamente");
                }//catch
                
        }while(s);
        
        return 0;
        
	}//validar

	public static int validarEntradaJ(){ //Valida el ingreso del nombre del nodo
		
        boolean s = true; int num;
        
        do{
                try{
                        num = read.nextInt();
                        if(num >= 0){
                                //System.out.println("Valor valido");
                                s = false;
                                return num;
                        }else
                                System.out.println("Numero invalido, ingrese nuevamente");
                }catch(Exception e){
                        read.next();
                        System.out.println("Dato invalido, ingrese nuevamente");
                }//catch
                
        }while(s);
        
        return 0;
        
	}//validar
	
	public static void quickSort() {
		ordenar(0,players-1);
	}
	
	public static void ordenar(int izq, int der) {
		MPunArb auxA = new MPunArb();
		Jugador auxJ = new Jugador();
		int i = izq, j = der, v = arArb[(i + j)/2].getPuntos();
		do {
			while(arArb[i].getPuntos() < v) {
				i = i+1;
			}
			while(arArb[j].getPuntos() > v) {
				j = j-1;
			}
			if( i <= j) {
				auxA = arArb[i]; auxJ = arJugadores[i];
				arArb[i] = arArb[j]; arJugadores[i] = arJugadores[j];
				arArb[j] = auxA; arJugadores[j] = auxJ;
				i = i + 1; j = j - 1;
			}
		}while(i <= j);
		if(izq < j) ordenar(izq, j);
		if(i < der) ordenar(i, der);
	}
	
}//Class Desarrollo
